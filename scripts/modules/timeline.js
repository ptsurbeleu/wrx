import u from "../umbrella/module.js"

(function(document, u) {
  "use strict";

  let x = u(".tweet-timeline");

  // Appends a new item to the beginning of the list
  function append(twid) {
    let item = u("<li>");
    tweet(twid, item.first());
    x.prepend(item);
    // schedule another item
    setTimeout(append, 25000, twid);
  }

  // Here is our first item
  append('1097413392371982336');
})(this, u);